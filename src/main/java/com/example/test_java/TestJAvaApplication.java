package com.example.test_java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestJAvaApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestJAvaApplication.class, args);
    }

}
